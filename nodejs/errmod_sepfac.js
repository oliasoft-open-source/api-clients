const fs = require('fs');
const axios = require('axios');

const DEFAULT_APIURL = 'https://welldesign.oliasoft.com/api';

const ERRMOD_SEPFAC_BODY = {
  site_datum: 'wgs84',
  site_loc: 'UTM 32 N 596945.749 6642871.404',
  resolution: '30|m',
  wells: [
    {
      "coordinates": {
        "datum": "wgs84",
        "loc": "UTM 32 N 596945.749 6642871.404"
      },
      "points": [
        ["m", "deg", "deg"],
        [0, 0, 0],
        [500, 0, 45],
        [1000, 60, 0]
      ],
      "holetable": [
        ["m", "m", "m"],
        [0, 10000, 0.4572]
      ],
      "errmodels": [{
        "from": "0|m",
        "to": "1000|m",
        "name": "dummymodel"
      }]
    },
    {
      "coordinates": {
        "datum": "wgs84",
        "loc": "UTM 32 N 596945.749 6642971.404"
      },
      "points": [
        ["m", "deg", "deg"],
        [0, 0, 0],
        [400, 10, 50],
        [1000, 40, 90]
      ],
      "holetable": [
        ["m", "m", "in"],
        [0, 10000, 12]
      ],
      "errmodels": [{
        "from": "0|m",
        "to": "1000|m",
        "name": "dummymodel"
      }]
    },
    {
      "coordinates": {
        "datum": "wgs84",
        "loc": "UTM 32 N 597145.749 6642871.404"
      },
      "points": [
        ["m", "deg", "deg"],
        [0, 0, 0],
        [600, 30, -60],
        [1200, 50, 50]
      ],
      "errmodels": [{
        "from": "0|m",
        "to": "1200|m",
        "name": "dummymodel2"
      }]
    }
  ],
  errmodeldefs: {
    "dummymodel": `#My dummy model
DSFS	e	s	m	0.055	tmd
XYM1r4	i	s	d	3	abs(sin(inc))
XYM3r4	a	s	d	3	abs(sin(inc))`,
    "dummymodel2": `#My dummy model
DSFS	e	s	m	0.055	tmd
XYM1r4	i	s	d	3	abs(sin(inc))
XYM3r4	a	s	d	3	abs(sin(inc))`
  },
  params: {
    "decl": 0,
    "dip": 70,
    "bfield": 50000,
    "gfield": 9.80665,
    "erot": 7.2921159E-5,
    "s_m": 0.3,
    "scale_fac": 3.5,
    "sigma_pa": 0.5,
    "r_r": 0.4572,
    "r_o": 0.3048
  }
};

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.message);
});

function errorExit(...args) {
  console.error(...args);
  process.exit(1);
}

async function main() {
  let {APIKEY, APIURL} = process.env;
  if (!APIKEY) errorExit('missing APIKEY');
  if (!APIURL) {
    APIURL = DEFAULT_APIURL;
    console.log('No APIURL found, using default', APIURL);
  }
  const url = APIURL + '/errmod_sepfac';
  console.log('posting to url:', url);
  const response = await axios.post(url,
    ERRMOD_SEPFAC_BODY, {
      withCredentials: true,
      headers: {
        Authorization: 'ApiKey ' + APIKEY,
      }
  });

  const {data} = response;
  if (!data || !data.sepfac) errorExit('invalid/unknown response');

  if (data.sepfac.length)
    console.log(data.sepfac);
  console.log('got valid response for the following number of offset wells:',
    data.sepfac.length);

  const RE = /^data:.+\/(.+);base64,(.*)$/;
  for (let key of ['minDistPlot', 'minSFPlot']) {
    if (data[key]) {
      const [_, ext, base64data] = data[key].match(RE);
      const buffer = Buffer.from(base64data, 'base64');
      const fileName = key + '.' + ext;
      console.log('writing chart file:', fileName);
      fs.writeFileSync(fileName, buffer);
    } else {
      console.log('skipped writing missing chart:', key);
    }
  }
}

main();
