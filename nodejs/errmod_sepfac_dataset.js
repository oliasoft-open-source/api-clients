const fs = require('fs');
const axios = require('axios');

const DEFAULT_APIURL = 'https://welldesign.oliasoft.com/api';

const ERRMOD_SEPFAC_DATASET_BODY = {
  datasetid: 28512
};

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.message);
});

function errorExit(...args) {
  console.error(...args);
  process.exit(1);
}

async function main() {
  let {APIKEY, APIURL, DATASETID} = process.env;
  if (!APIKEY) errorExit('missing APIKEY');
  if (!APIURL) {
    APIURL = DEFAULT_APIURL;
    console.log('No APIURL found, using default', APIURL);
  }
  if (DATASETID) ERRMOD_SEPFAC_DATASET_BODY.datasetid = DATASETID;

  const url = APIURL + '/errmod_sepfac_dataset';
  console.log('posting to url:', url);
  const response = await axios.post(url,
    ERRMOD_SEPFAC_DATASET_BODY, {
      withCredentials: true,
      headers: {
        Authorization: 'ApiKey ' + APIKEY,
      }
  });

  const {data} = response;
  if (!data || !data.sepfac) errorExit('invalid/unknown response');

  console.log('got valid response for the following number of offset wells:',
    data.sepfac.length);

  const RE = /^data:.+\/(.+);base64,(.*)$/;
  for (let key of ['minDistPlot', 'minSFPlot']) {
    if (data[key]) {
      const [_, ext, base64data] = data[key].match(RE);
      const buffer = Buffer.from(base64data, 'base64');
      const fileName = key + '.' + ext;
      console.log('writing chart file:', fileName);
      fs.writeFileSync(fileName, buffer);
    }
  }
  console.log(JSON.stringify(data));
}

main();
