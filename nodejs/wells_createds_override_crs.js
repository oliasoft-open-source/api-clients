const fs = require('fs');
const axios = require('axios');

const DEFAULT_APIURL = 'https://welldesign.oliasoft.com/api';

// Please note that if error model information is missing, or name
// points to a non-existing error model, a default MWDr4 model will
// get added by default. This is to simplify getting started with
// these fairly complex APIs.

// Changes a "unique" id that changes every hour, to make it
// easy to simulate both inserts and updates:
const fake_id = (new Date()).toISOString().substr(0, 13);

/**
 * Body with fieldSettings and siteSettings for overriding the default
 * CRS applied to the field and site in the "browse" projects view.
 */
const BODY = {
  "name": 'Sepfac Create Test ' + fake_id + '123123123',
  "country": "NO",
  "field": 'SFPOC',
  "fieldSettings": {
      "crs": {
        "datum": "wgs84",
        "format": "UTM 32 N"
      }
  },
  "siteSettings": {
    "coordinates": {
      "datum": "wgs84",
      "loc": "UTM 32 N 596945.749 6642871.404"
    }
  },
  "site": "SFPOC",
  "well": "SFPOC",
  "wellbore": "SFPOC",
  "ext_system_name": "APIClient",
  "ext_system_id": fake_id,
  "dataset": {
    "errmod_sepfac": {
      "wells": [
        {
          "name": "Parametric reference",
          "well_id": "parametric_reference",
          "resolution": "30|m",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 596945.749 6642871.404"
          },
          "model": [
            {"md": "m", "cl": "m", "ew": "m", "ns": "m", "tf": "deg", "tr": "deg/30m", "br": 'deg/30m', "azi": "deg", "dls": "deg/30m", "inc": "deg", "tvd": "m", "type": "Units"},
            {"type": "Tie Line", "cl": "0", "md": "0", "tvd": "0", "ew": "0", "ns": "0", "inc": "0", "azi": "0"},
            {"type": "Line TVD", "tvd": "525"},
            {"type": "BT3 Inc", "inc": "-25", "br": "-1", "tr": "2.5"}
          ],
          "holetable": [
            ["m", "m", "m"],
            [0, 10000, 0.4572]]
        },
        {
          "name": "Survey offset 1",
          "well_id": "survey_offset_1",
          "resolution": "30|m",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 596945.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [500, 0, 45],
            [1000, 60, 0]
          ],
          "holetable": [
            ["m", "m", "m"],
            [0, 10000, 0.4572]]
        },
        {
          "name": "Survey offset 2",
          "well_id": "survey_offset_2",
          "resolution": "30|m",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 596945.749 6642971.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [400, 10, 50],
            [1000, 40, 90]
          ],
          "holetable": [
            ["m", "m", "in"],
            [0, 10000, 12]
          ]
        },
        {
          "name": "Survey offset 3",
          "well_id": "survey_offset_3",
          "resolution": "30|m",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 597145.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [600, 30, -60],
            [1200, 50, 50]
          ],
        },
        {
          "name": "Survey Kickoff from Parametric reference",
          "id": "srv_ko_from_param_ref",
          "resolution": "30|m",
          "kickoffix": 0,
          "kickoffmd": "700|m",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 597145.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [700, -30, 60],
            [1000, -50, -50]
          ]
        },
        {
          "name": "Parametric Kickoff from Survey Offset 3",
          "id": "param_ko_from_srv_ofs_3",
          "resolution": "30|m",
          "kickoffix": 3,
          "kickoffmd": "800|m",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 597145.749 6642871.404"
          },
          "model": [
            {"md": "m", "cl": "m", "ew": "m", "ns": "m", "tf": "deg", "tr": "deg/30m", "br": 'deg/30m', "azi": "deg", "dls": "deg/30m", "inc": "deg", "tvd": "m", "type": "Units"},
            {"type": "Tie Line", "cl": "0", "md": "0", "tvd": "0", "ew": "0", "ns": "0", "inc": "0", "azi": "0"},
            {"type": "BT3 Inc", "inc": "-25", "br": "-1", "tr": "2.5"}
          ],
        }
      ],
      "targets": [
        [-300, 300, "3280.84|ft", "Entry 1"],
        [-500, "700|m", 1100, "Target 1"]
      ],
      "errmodeldefs": {
        "dummymodel": `#My dummy model
DSFS	e	s	m	0.055	tmd
XYM1r4	i	s	d	3	abs(sin(inc))
XYM3r4	a	s	d	3	abs(sin(inc))`,
        "dummymodel2": `#My dummy model
DSFS	e	s	m	0.055	tmd
XYM1r4	i	s	d	3	abs(sin(inc))
XYM3r4	a	s	d	3	abs(sin(inc))`
      }
    }
  }
};

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.message, error?.response?.data);
});

function errorExit(...args) {
  console.error(...args);
  process.exit(1);
}

async function main() {
  let {APIKEY, APIURL} = process.env;
  if (!APIKEY) errorExit('missing APIKEY');
  if (!APIURL) {
    APIURL = DEFAULT_APIURL;
    console.log('No APIURL found, using default', APIURL);
  }
  // const url = APIURL + '/createds';
  const url = APIURL + '/upsertObjectsExt';
  console.log('posting:', {url, ext_system_name: BODY.ext_system_name, ext_system_id: BODY.ext_system_id});
  const response = await axios.post(url,
    BODY, {
      withCredentials: true,
      headers: {
        Authorization: 'ApiKey ' + APIKEY,
      }
  });

  const {data} = response;

  console.log('returned', data);
}

main();
