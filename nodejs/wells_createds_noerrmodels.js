const fs = require('fs');
const axios = require('axios');

const DEFAULT_APIURL = 'https://welldesign.oliasoft.com/api';

// Please note that if error model information is missing, or name
// points to a non-existing error model, a default MWDr4 model will
// get added by default. This is to simplify getting started with
// these fairly complex APIs.

// Changes a "unique" id that changes every hour, to make it
// easy to simulate both inserts and updates:
const fake_id = (new Date()).toISOString().substr(0, 13);

const BODY = {
  "name": 'Sepfac Create Test ' + fake_id,
  "country": "NO",
  "field": 'SFPOC',
  "site": "SFPOC",
  "well": "SFPOC",
  "wellbore": "SFPOC",
  "ext_system_name": "APIClient",
  "ext_system_id": fake_id,
  "dataset": {
    "errmod_sepfac": {
      "wells": [
        {
          "name": "My reference",
          "well_id": "my_ref",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 596945.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [500, 0, 45],
            [1000, 60, 0]
          ],
          "holetable": [
            ["m", "m", "m"],
            [0, 10000, 0.4572]],
        },
        {
          "name": "An offset well",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 596945.749 6642971.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [400, 10, 50],
            [1000, 40, 90]
          ],
          "holetable": [
            ["m", "m", "in"],
            [0, 10000, 12]
          ],
        },
        {
          "name": "Another offset well",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 597145.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [0, 0, 0],
            [600, 30, -60],
            [1200, 50, 50]
          ],
        },
        {
          "name": "Kickoff from ref",
          "id": "my_kickoff",
          "kickoffix": 0,
          "kickoffmd": "700|m",
          "coordinates": {
            "datum": "wgs84",
            "loc": "UTM 32 N 597145.749 6642871.404"
          },
          "points": [
            ["m", "deg", "deg"],
            [700, -30, 60],
            [1000, -50, -50]
          ],
        }
      ],
      "targets": [
        [-300, 300, "3280.84|ft", "Entry 1"],
        [-500, "700|m", 1100, "Target 1"]
      ],
    }
  }
};

process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  console.log('unhandledRejection', error.message, error?.response?.data);
});

function errorExit(...args) {
  console.error(...args);
  process.exit(1);
}

async function main() {
  let {APIKEY, APIURL} = process.env;
  if (!APIKEY) errorExit('missing APIKEY');
  if (!APIURL) {
    APIURL = DEFAULT_APIURL;
    console.log('No APIURL found, using default', APIURL);
  }
  // const url = APIURL + '/createds';
  const url = APIURL + '/upsertObjectsExt';
  console.log('posting:', {url, ext_system_name: BODY.ext_system_name, ext_system_id: BODY.ext_system_id});
  const response = await axios.post(url,
    BODY, {
      withCredentials: true,
      headers: {
        Authorization: 'ApiKey ' + APIKEY,
      }
  });

  const {data} = response;

  console.log('returned', data);
}

main();
